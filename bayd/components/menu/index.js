import React,{Component} from 'react'
import PropTypes from 'prop-types'
import styles from './style.scss'

export default class Menu extends Component{
	static propTypes = {
		menuList: PropTypes.array,
		changeMenu: PropTypes.func,
		defaultMenu: PropTypes.string,
	}

	constructor(props){
		super(props)
		this.state={
			activeMenu:''
		}
	}
	componentDidMount(){
		const {defaultMenu=''}=this.props
		if(!defaultMenu) return
		this.changeMenu(defaultMenu)
	}
    
    changeMenu = (url) => {
    	if(!url) return
    	this.setState({
    		activeMenu:url
    	},()=>{
    		this.props.changeMenu(url)
    	})
    }
	renderMenu = (menuList) => {
		let {activeMenu}=this.state
		return menuList.map(item=>{
			return(<div key={item.key} className={ activeMenu==item.key ? styles['active'] : ''} 
				onClick={this.changeMenu.bind(this,item.key)}
				title={item.text}
			>{item.text}</div>)
		})
	}
    
	render(){
		const {menuList=[]} = this.props
    	return <div className={styles['wrap']}>
			{this.renderMenu(menuList)}
    		{/* <div className={activeMenu=='/componentList/modal'?styles['active']:''} 
    			onClick={this.changeMenu.bind(this,'/componentList/modal')}
    			title='Modal弹出框'
    		>Modal弹出框</div>

    		<div className={activeMenu=='/componentList/chart'?styles['active']:''} 
    			onClick={this.changeMenu.bind(this,'/componentList/chart')}
    			title='Chart柱状图'
    		>Chart柱状图</div> */}
    	</div>
	}
}