import React,{Component} from 'react'
import PropTypes from 'prop-types'
import styles from './style.scss'
import AceEditor from 'react-ace'

import 'ace-builds/src-noconflict/mode-javascript'
// import 'ace-builds/src-noconflict/theme-cobalt'
// import 'ace-builds/src-noconflict/theme-iplastic'
// import 'ace-builds/src-noconflict/theme-solarized_light'
// import 'ace-builds/src-noconflict/theme-tomorrow_night_blue'
import 'ace-builds/src-noconflict/theme-cobalt'



export default class Card extends Component{
	static propTypes = {
		renderView: PropTypes.func,
		description: PropTypes.string,
		code: PropTypes.string,
	}
	constructor(){
		super()
		this.state={
			displayCode:false
		}
	}
	changeDisplayCode = () => {
		this.setState({
			displayCode:!this.state.displayCode
		})
	}
	render(){
		let {displayCode}=this.state
		let {renderView,description,code}=this.props
		return<div className={styles['card-wrap']}>
			<div className={styles['view']}>
				{
					renderView && renderView()	
				}
			</div>
			<div className={styles['code']}>
				<div className={styles['description']}>
					{description}
					<a onClick={this.changeDisplayCode}>展开</a>
				</div>
				<AceEditor
					style={{width:'100%',display:displayCode?'block':'none'}}
					mode='javascript'
					theme='cobalt'
					value={code}
				/>
			</div>
		</div>
	}
}