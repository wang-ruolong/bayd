const path = require('path')
const glob = require('glob')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

// 构造多入口，每个组件一个入口，每个组件单独打包，这样可以让引用方按需加载。
const list = {}
async function makeList(dirPath,list){
    const files = glob.sync(`${dirPath}/**/index.js`)
    for(let file of files){
        const component = file.split(/[/.]/)[2]
        list[component] = `./${file}`
    }
}
makeList('bayd/components',list)

module.exports = {
    mode:"development",
    devtool:"source-map",
    entry: list,
    output:{
        filename: '[name]/index.js',
        path: path.join(__dirname,'lib'),
        libraryTarget: 'umd',
        library: 'mui'
    },
    plugins:[
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
			filename: 'index.css'
		}),
    ],
    module:{
        rules:[
            {
				exclude: /node_modules/,
				test: /\.(js|jsx)$/,
				loader: ['babel-loader']
			},
            {
				test: /\.(sc|c)ss$/,
				use: [
					// MiniCssExtractPlugin.loader,
                    // 这里不能用MiniCssExtractPlugin，把css抽取出来。因为如果这样做的话，当在引用方要import css进去的时候，css路径就会变化，默认情况下会加上文件的前缀也就是.node_modules-bayd-components-button-style__button-box
                    // 与我们这边实际的路径.bayd-components-button-style__button-box对不上，会导致样式出不来。
                    // 我尝试了各种方法，最后觉得用 style-loader 能比较好的解决这个问题。
                    // style-loader的工作原理是在html的<head></head>那边加入<style></style>，这样的话只有当组件有被import，才会生成<style></style>，相当于就是按需加载。
                    'style-loader',
					{
						loader: 'css-loader',
						options: {
							modules: true,
							localIdentName: '[path][name]__[local]--[hash:base64:5]'
						}
					},
					{
						loader: 'sass-loader'
					}
				],
			},
        ]
    }
}